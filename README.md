# Giphy Browser

This project is a small application that can load GIFs from https://giphy.com/

#### Features

- Displays treanding gifs from https://giphy.com/ on the initial page load.
- The ability to search for GIFs via a search bar on top. (These
results will replace the trending GIFs).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
