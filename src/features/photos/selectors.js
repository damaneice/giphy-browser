export const getPhotoGrid = state => state.photo.grid;
export const getPhotoOffset = state => state.photo.offset;
