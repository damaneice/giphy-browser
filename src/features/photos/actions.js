import {
  CREATE_PHOTO_GRID,
  RELOAD_IMAGES,
  RESIZE_GRID,
  HIDE_TOP_IMAGES,
  SHOW_TOP_IMAGES,
  SET_ENDPOINT,
  CLEAR_IMAGES,
} from './actionTypes';

export const loadImages = (
  giphyData,
  margin,
  containerWidth,
  targetRowHeight,
  offset
) => ({
  giphyData,
  margin,
  containerWidth,
  targetRowHeight,
  offset,
  type: CREATE_PHOTO_GRID,
});

export const reloadImages = () => {
  return {type: RELOAD_IMAGES};
};

export const hideTopImages = () => {
  return {type: HIDE_TOP_IMAGES};
};

export const showTopImages = () => {
  return {type: SHOW_TOP_IMAGES};
};

export const resizeGrid = containerWidth => {
  return {type: RESIZE_GRID, containerWidth};
};

export const clearImages = () => {
  return {type: CLEAR_IMAGES};
};

export const setEndpoint = endpoint => {
  return {type: SET_ENDPOINT, endpoint};
};
