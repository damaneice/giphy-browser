import {
  CREATE_PHOTO_GRID,
  HIDE_TOP_IMAGES,
  SHOW_TOP_IMAGES,
  RESIZE_GRID,
} from './actionTypes';
import PhotoReducer, {initialState} from './PhotoReducer';

describe('features > photo > PhotoReducer', () => {
  it('returns initial state, if non matched action is provided', () => {
    const initialState = {
      grid: [],
    };

    const action = {
      type: 'FOO',
    };

    expect(PhotoReducer(initialState, action)).toBe(initialState);
  });

  it(`Return photo grid, if ${CREATE_PHOTO_GRID} action is provided`, () => {
    const newInitialState = {
      ...initialState,
      grid: [],
      giphys: [
        {
          title: 'Sade',
          images: {
            fixed_height_downsampled: {
              height: '200',
              url: 'https://media0.giphy.com/3434/ahW/200.gif',
              width: '500',
            },
            downsized_medium: {
              height: '321',
              url: 'https://media0.giphy.com/media/x3434TcnhW/324.gif',
              width: '600',
            },
          },
        },
      ],
    };

    const expectedState = {
      endpoint:
        'https://api.giphy.com/v1/gifs/trending?api_key=8ok6WmBxT1zOQpDh7IV5OjVx1N1dB1ls',
      grid: [
        {
          rowHeightStartPosition: 0,
          height: 156,
          images: [
            {
              xStartPosition: 0,
              title: 'Sade',
              height: 156,
              width: 391,
              url: 'https://media0.giphy.com/3434/ahW/200.gif',
              largeImageUrl:
                'https://media0.giphy.com/media/x3434TcnhW/324.gif',
              originalHeight: '200',
              originalLargeImageHeight: '321',
              originalLargeImageWidth: '600',
              originalWidth: '500',
            },
            {
              xStartPosition: 391,
              title: 'you mad GIF',
              height: 156,
              width: 208,
              url: 'https://media0.giphy.com/media/xTcnSOEKegBnYhGahW/200.gif',
              largeImageUrl: 'https://media0.giphy.com/media/xTcnhW/400.gif',
              originalHeight: '200',
              originalLargeImageHeight: '240',
              originalLargeImageWidth: '300',
              originalWidth: '267',
            },
          ],
        },
      ],
      giphys: [
        {
          title: 'Sade',
          images: {
            fixed_height_downsampled: {
              height: '200',
              url: 'https://media0.giphy.com/3434/ahW/200.gif',
              width: '500',
            },
            downsized_medium: {
              height: '321',
              url: 'https://media0.giphy.com/media/x3434TcnhW/324.gif',
              width: '600',
            },
          },
        },
        {
          title: 'you mad GIF',
          images: {
            fixed_height_downsampled: {
              height: '200',
              url: 'https://media0.giphy.com/media/xTcnSOEKegBnYhGahW/200.gif',
              width: '267',
            },
            downsized_medium: {
              height: '240',
              url: 'https://media0.giphy.com/media/xTcnhW/400.gif',
              width: '300',
            },
          },
        },
      ],
      margin: 0,
      containerWidth: 600,
      targetRowHeight: 200,
      gridViewPortHeight: 0,
      offset: 1,
      totalGiphyCount: 2,
    };

    const action = {
      type: CREATE_PHOTO_GRID,
      targetRowHeight: 200,
      offset: 1,
      giphyData: {
        data: [
          {
            title: 'you mad GIF',
            images: {
              fixed_height_downsampled: {
                height: '200',
                url:
                  'https://media0.giphy.com/media/xTcnSOEKegBnYhGahW/200.gif',
                width: '267',
              },
              downsized_medium: {
                height: '240',
                url: 'https://media0.giphy.com/media/xTcnhW/400.gif',
                width: '300',
              },
            },
          },
        ],
      },
      containerWidth: 600,
      margin: 0,
    };

    expect(PhotoReducer(newInitialState, action)).toEqual(expectedState);
  });

  it(`Removes the last 30 percent of image rows from the photo grid, if the ${HIDE_TOP_IMAGES} is provided`, () => {
    const giphyData = {
      title: 'Sade',
      images: {
        fixed_height_downsampled: {
          height: '200',
          url: 'https://media0.giphy.com/3434/ahW/200.gif',
          width: '500',
        },
        downsized_medium: {
          height: '321',
          url: 'https://media0.giphy.com/media/x3434TcnhW/324.gif',
          width: '600',
        },
      },
    };
    const newInitialState = {
      ...initialState,
      containerWidth: 400,
      targetRowHeight: 200,
      giphys: [
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
        giphyData,
      ],
    };

    const action = {
      type: HIDE_TOP_IMAGES,
    };
    const result = PhotoReducer(newInitialState, action);

    expect(result.grid.length).toBe(5);
  });

  it(`Display 30 percent of the hidden image rows on the photo grid, if the ${SHOW_TOP_IMAGES} is provided`, () => {
    const giphyData = {
      title: 'Sade',
      images: {
        fixed_height_downsampled: {
          height: '200',
          url: 'https://media0.giphy.com/3434/ahW/200.gif',
          width: '500',
        },
        downsized_medium: {
          height: '321',
          url: 'https://media0.giphy.com/media/x3434TcnhW/324.gif',
          width: '600',
        },
      },
    };
    const giphys = [
      {...giphyData, title: 'snowboarding is fun'},
      {...giphyData, title: 'racing'},
      {...giphyData, title: 'basketball is fun'},
      {...giphyData, title: 'fishing is fun'},
      {...giphyData, title: 'snowboarding is fun'},
      {...giphyData, title: 'racing'},
      {...giphyData, title: 'basketball is fun'},
      {...giphyData, title: 'fishing is fun'},
      {...giphyData, title: 'snowboarding is fun'},
      {...giphyData, title: 'racing'},
      {...giphyData, title: 'basketball is fun'},
      {...giphyData, title: 'fishing is fun'},
      giphyData,
      giphyData,
      giphyData,
      giphyData,
      giphyData,
      giphyData,
    ];

    const newInitialState = {
      ...initialState,
      containerWidth: 400,
      targetRowHeight: 200,
      giphys,
    };

    let action = {
      type: HIDE_TOP_IMAGES,
      targetRowHeight: 200,
      giphyData: {
        data: giphys,
      },
      containerWidth: 400,
      margin: 0,
    };

    const hidePhotosdResult = PhotoReducer(newInitialState, action);

    expect(hidePhotosdResult.grid.length).toBe(7);
    expect(hidePhotosdResult.gridViewPortHeight).toBe(0);

    action = {
      type: SHOW_TOP_IMAGES,
    };

    const result = PhotoReducer(newInitialState, action);

    expect(result.grid.length).toBe(9);
    expect(result.gridViewPortHeight).toBe(640);
  });

  it(`Resize images and photo grid, if the ${RESIZE_GRID} is provided`, () => {
    const giphyData = {
      title: 'Sade',
      images: {
        fixed_height_downsampled: {
          height: '200',
          url: 'https://media0.giphy.com/3434/ahW/200.gif',
          width: '500',
        },
        downsized_medium: {
          height: '321',
          url: 'https://media0.giphy.com/media/x3434TcnhW/324.gif',
          width: '600',
        },
      },
    };
    const giphys = [
      {...giphyData, title: 'snowboarding is fun'},
      {...giphyData, title: 'racing'},
      {...giphyData, title: 'basketball is fun'},
      {...giphyData, title: 'fishing is fun'},
      {...giphyData, title: 'snowboarding is fun'},
      {...giphyData, title: 'racing'},
      giphyData,
      giphyData,
      giphyData,
      giphyData,
      giphyData,
      giphyData,
    ];

    let newInitialState = {
      ...initialState,
      containerWidth: 400,
      targetRowHeight: 200,
    };

    let action = {
      type: CREATE_PHOTO_GRID,
      targetRowHeight: 200,
      giphyData: {
        data: giphys,
        pagination: {offset: 0},
      },
      containerWidth: 400,
      margin: 0,
      pagination: {offset: 0},
    };

    const createdPhotoGridResult = PhotoReducer(newInitialState, action);

    expect(createdPhotoGridResult.grid.length).toBe(6);
    expect(createdPhotoGridResult.gridViewPortHeight).toBe(400);

    newInitialState = {
      ...initialState,
      giphys,
      containerWidth: 400,
      targetRowHeight: 200,
    };

    action = {
      type: RESIZE_GRID,
      containerWidth: 800,
    };

    const result = PhotoReducer(newInitialState, action);

    expect(result.grid.length).toBe(4);
    expect(result.gridViewPortHeight).toBe(318);
  });
});
