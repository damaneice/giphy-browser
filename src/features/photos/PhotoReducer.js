import {
  CREATE_PHOTO_GRID,
  RELOAD_IMAGES,
  SHOW_TOP_IMAGES,
  HIDE_TOP_IMAGES,
  RESIZE_GRID,
  SET_ENDPOINT,
  CLEAR_IMAGES,
} from './actionTypes';
import {getGridRows} from '../../service/photoGridService';

export const initialState = {
  endpoint:
    'https://api.giphy.com/v1/gifs/trending?api_key=8ok6WmBxT1zOQpDh7IV5OjVx1N1dB1ls',
  grid: [],
  giphys: [],
  totalGiphyCount: 0,
  offset: -1,
  margin: 0,
  containerWidth: 0,
  targetRowHeight: 0,
  gridViewPortHeight: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PHOTO_GRID: {
      const {giphys, grid} = createPhotoGrid(
        state.giphys,
        action.giphyData.data,
        action.containerWidth,
        action.margin,
        action.targetRowHeight
      );

      return {
        ...state,
        grid,
        giphys,
        totalGiphyCount: giphys.length,
        offset: action.offset,
        margin: action.margin,
        containerWidth: action.containerWidth,
        targetRowHeight: action.targetRowHeight,
        gridViewPortHeight:
          grid.length > 0 ? grid[grid.length - 1].rowHeightStartPosition : 0,
      };
    }
    case RELOAD_IMAGES: {
      const images = extractImagesFromGiphyObject(state.giphys);
      let grid = getGridRows(
        images,
        state.containerWidth,
        state.margin,
        state.targetRowHeight
      );
      const lastVisibleRowIndex = grid.findIndex(row => {
        return JSON.stringify(row) === JSON.stringify(state.grid[0]);
      });

      if (lastVisibleRowIndex > grid.length) {
        grid = grid.slice(lastVisibleRowIndex);
      }
      return {
        ...state,
        grid,
        gridViewPortHeight:
          grid.length > 0 ? grid[grid.length - 1].rowHeightStartPosition : 0,
      };
    }
    case HIDE_TOP_IMAGES: {
      const images = extractImagesFromGiphyObject(state.giphys);
      let grid = getGridRows(
        images,
        state.containerWidth,
        state.margin,
        state.targetRowHeight
      );

      if (grid.length > 0) {
        grid = grid.slice(parseInt(grid.length * 0.3, 10));
      }

      return {
        ...state,
        grid: grid.slice(0, grid.length),
      };
    }
    case SHOW_TOP_IMAGES: {
      const images = extractImagesFromGiphyObject(state.giphys);
      let grid = getGridRows(
        images,
        state.containerWidth,
        state.margin,
        state.targetRowHeight
      );

      const lastVisibleRowIndex = grid.findIndex(row => {
        return JSON.stringify(row) === JSON.stringify(state.grid[0]);
      });

      const numberOfHiddenRows = lastVisibleRowIndex + 1;
      if (numberOfHiddenRows / grid.length > 3 / 10) {
        grid = grid.slice(
          parseInt(
            grid.length * (numberOfHiddenRows / grid.length - 3 / 10),
            10
          )
        );
      }
      return {
        ...state,
        grid,
        gridViewPortHeight:
          grid.length > 0 ? grid[grid.length - 1].rowHeightStartPosition : 0,
      };
    }
    case RESIZE_GRID: {
      const images = extractImagesFromGiphyObject(state.giphys);
      const grid = getGridRows(
        images,
        action.containerWidth,
        state.margin,
        state.targetRowHeight
      );

      return {
        ...state,
        containerWidth: action.containerWidth,
        grid,
        gridViewPortHeight:
          grid.length > 0 ? grid[grid.length - 1].rowHeightStartPosition : 0,
      };
    }
    case CLEAR_IMAGES: {
      return {
        ...state,
        grid: [],
        giphys: [],
        gridViewPortHeight: 0,
      };
    }
    case SET_ENDPOINT: {
      return {
        ...state,
        endpoint: action.endpoint,
      };
    }
    default:
      return state;
  }
};

const extractImagesFromGiphyObject = existingGiphys => {
  return existingGiphys.map(giphyObject => {
    return {
      ...giphyObject.images.fixed_height_downsampled,
      largeImageUrl: giphyObject.images.downsized_medium.url,
      largeImageWidth: giphyObject.images.downsized_medium.width,
      largeImageHeight: giphyObject.images.downsized_medium.height,
      title: giphyObject.title,
    };
  });
};

const createPhotoGrid = (
  existingGiphys,
  newGiphys,
  containerWidth,
  margin,
  targetRowHeight
) => {
  const giphys = existingGiphys.concat(newGiphys);
  const images = extractImagesFromGiphyObject(giphys);
  let grid = getGridRows(images, containerWidth, margin, targetRowHeight);
  const lastVisibleRowIndex = grid.findIndex(row => {
    return JSON.stringify(row) === JSON.stringify(existingGiphys[0]);
  });

  if (lastVisibleRowIndex > grid.length) {
    grid = grid.slice(lastVisibleRowIndex);
  }
  return {giphys, grid};
};
