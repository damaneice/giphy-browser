import {getGridRows} from './photoGridService';

it('create grid with one row with a width close 600px', () => {
  const photos = [
    {width: 300, height: 200},
    {width: 400, height: 200},
    {width: 200, height: 150},
  ];
  const rows = getGridRows(photos, 600, 0, 200);

  expect(rows.length).toEqual(1);
  expect(rows[0].images.length).toEqual(3);
  expect(getRowWidth(rows[0].images)).toEqual(599);
  expect(rows[0].height).toEqual(124);
  expect(rows[0].rowHeightStartPosition).toEqual(0);
});

it('create grid with one row with a portrait and landscape image with a width close 400px', () => {
  const photos = [
    {width: 400, height: 600},
    {width: 400, height: 200},
  ];
  const rows = getGridRows(photos, 400, 0, 200);

  expect(rows.length).toEqual(1);
  expect(rows[0].images.length).toEqual(2);
  expect(getRowWidth(rows[0].images)).toEqual(400);
  expect(rows[0].height).toEqual(150);
  expect(rows[0].rowHeightStartPosition).toEqual(0);
});

it('create grid with one row with three portrait images with a width close 400px', () => {
  const photos = [
    {width: 120, height: 200},
    {width: 200, height: 400},
    {width: 300, height: 500},
  ];
  const rows = getGridRows(photos, 400, 0, 200);

  expect(rows.length).toEqual(1);
  expect(rows[0].images.length).toEqual(3);
  expect(getRowWidth(rows[0].images)).toEqual(399);
  expect(rows[0].height).toEqual(235);
  expect(rows[0].rowHeightStartPosition).toEqual(0);
});

it('create grid with one row with two large landscape images with a width close 400px', () => {
  const photos = [
    {width: 1200, height: 800},
    {width: 1024, height: 968},
  ];
  const rows = getGridRows(photos, 400, 0, 200);

  expect(rows.length).toEqual(1);
  expect(rows[0].images.length).toEqual(2);
  expect(getRowWidth(rows[0].images)).toEqual(399);
  expect(rows[0].height).toEqual(156);
  expect(rows[0].rowHeightStartPosition).toEqual(0);
});

it('create grid with images with the horizational pixel count for absolute positioning', () => {
  const photos = [
    {width: 1200, height: 800},
    {width: 1024, height: 968},
  ];
  const rows = getGridRows(photos, 400, 0, 200);
  const firstImage = rows[0].images[0];
  const secondImage = rows[0].images[1];

  expect(rows.length).toEqual(1);
  expect(rows[0].images.length).toEqual(2);
  expect(firstImage.xStartPosition).toEqual(0);
  expect(secondImage.xStartPosition).toEqual(234);
});

const getRowWidth = row =>
  row.reduce(function(accumulator, currentValue) {
    return accumulator + currentValue.width;
  }, 0);
