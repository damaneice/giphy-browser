export const getGridRows = (
  photos,
  containerWidth,
  margin,
  targetRowHeight
) => {
  const minAspectRatio = (containerWidth / targetRowHeight) * 0.8;
  let totalRatio = 0;
  let previousDiff = targetRowHeight;
  let rows = [];
  let images = [];
  let index = 0;
  let rowHeightStartPosition = 0;
  let xStartPosition = 0;

  for (const photo of photos) {
    totalRatio += photo.width / photo.height;

    const h = containerWidth / totalRatio;
    const currentDiff = Math.abs(h - photo.height);
    images = images.concat(photo);
    if (
      (previousDiff < currentDiff && totalRatio > minAspectRatio) ||
      index + 1 === photos.length
    ) {
      images = images.map(node => {
        const imageObj = {};
        const ratio = node.width / node.height;
        const height = h - margin;
        imageObj.height = parseInt(height, 10);
        imageObj.width = parseInt(ratio * height, 10);
        imageObj.url = node.url;
        imageObj.largeImageUrl = node.largeImageUrl;
        imageObj.title = node.title;
        imageObj.xStartPosition = xStartPosition;
        xStartPosition += imageObj.width + margin;
        imageObj.originalWidth = node.width;
        imageObj.originalHeight = node.height;
        imageObj.originalLargeImageWidth = node.largeImageWidth;
        imageObj.originalLargeImageHeight = node.largeImageHeight;

        return imageObj;
      });
      rows = rows.concat({
        images,
        rowHeightStartPosition,
        height: images[0].height,
      });
      rowHeightStartPosition += parseInt(images[0].height + margin, 10);
      xStartPosition = 0;
      images = [];
      totalRatio = 0;
      previousDiff = targetRowHeight;
    } else {
      previousDiff = currentDiff;
    }
    index += 1;
  }
  return rows;
};
