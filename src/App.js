import React from 'react';
import Grid from './components/Grid';
import Header from './components/Header';
import './index.scss';

const App = () => (
  <div className="app">
    <Header />
    <div className="container">
      <Grid />
    </div>
  </div>
);

export default App;
