import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
  loadImages,
  clearImages,
  setEndpoint,
} from '../../features/photos/actions';

const axios = require('axios').default;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.searchBox = React.createRef();
  }

  handleKeyDown = async e => {
    if (e.key === 'Enter') {
      await this.searchForGifs(this.searchBox.current.value);
      this.searchBox.current.value = '';
    }
  };

  handleSearchButton = async () => {
    const {value} = this.searchBox.current;
    if (value && value.length > 0) {
      await this.searchForGifs(this.searchBox.current.value);
      this.searchBox.current.value = '';
    }
  };

  searchForGifs = async searchTerm => {
    try {
      const url = `https://api.giphy.com/v1/gifs/search?api_key=8ok6WmBxT1zOQpDh7IV5OjVx1N1dB1ls&limit=50&q=${searchTerm}`;
      this.props.clearImages();
      this.props.setEndpoint(url);
      const response = await axios.get(url);
      this.props.loadImages(
        response.data,
        4,
        this.props.containerWidth,
        200,
        0
      );
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <div className="top-bar top-nav">
        <div className="top-bar-left">
          <h4>Giphy Browser</h4>
        </div>
        <div className="top-bar-right">
          <ul className="menu">
            <li>
              <input
                type="search"
                ref={this.searchBox}
                onKeyDown={this.handleKeyDown}
                placeholder="Search.."
              />
            </li>
            <li>
              <button
                onClick={this.handleSearchButton}
                type="button"
                className="button">
                Search
              </button>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
Header.propTypes = {
  loadImages: PropTypes.func,
  clearImages: PropTypes.func,
  setEndpoint: PropTypes.func,
  containerWidth: PropTypes.number,
};

const mapStateToProps = state => {
  return {
    containerWidth: state.photo.containerWidth,
    endpoint: state.photo.endpoint,
  };
};

const mapDispatchToProps = {
  loadImages,
  clearImages,
  setEndpoint,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
