import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  onClose = e => {
    this.props.onClose && this.props.onClose(e);
  };

  cssTransform = (scaleMax, percentageIncrease) => {
    return `scale(${scaleMax}) translate(${(100 * percentageIncrease) /
      2}%, ${(100 * percentageIncrease) / 2}%)`;
  };

  cssTop = (rectY, scaleMax, imageHeight) => {
    const distanceFromTop = window.scrollY + rectY;
    const containerHeight = window.innerHeight - distanceFromTop;
    return window.scrollY + (containerHeight - scaleMax * imageHeight) / 2;
  };

  scaleImage = () => {
    const {image, parentNode} = this.props;
    const imageWidth = image.originalLargeImageWidth;
    const imageHeight = image.originalLargeImageHeight;
    const widthScaleMax = parentNode.offsetWidth / imageWidth;
    const heightScaleMax = window.innerHeight / imageHeight;

    const style = {position: 'absolute', transition: '0.4s'};

    if (widthScaleMax < heightScaleMax) {
      if (widthScaleMax > 1) {
        const percentageIncrease =
          1 - imageWidth / (widthScaleMax * imageWidth);
        style.transform = this.cssTransform(widthScaleMax, percentageIncrease);
      }
      style.top = this.cssTop(
        parentNode.getBoundingClientRect().y,
        widthScaleMax,
        imageHeight
      );
      const left = (parentNode.offsetWidth - widthScaleMax * imageWidth) / 2;
      style.left = `${left}px`;
    } else {
      if (heightScaleMax > 1) {
        const percentageIncrease =
          1 - imageHeight / (heightScaleMax * imageHeight);
        style.transform = this.cssTransform(heightScaleMax, percentageIncrease);
      }
      const left = (parentNode.offsetWidth - heightScaleMax * imageWidth) / 2;
      style.left = `${left}px`;
      style.top = this.cssTop(
        parentNode.getBoundingClientRect().y,
        heightScaleMax,
        imageHeight
      );
    }

    return style;
  };

  render() {
    if (!this.props.show) {
      return null;
    }

    return (
      <div className="fullscreen-image" onClick={this.onClose}>
        <img style={this.scaleImage()} src={this.props.image.largeImageUrl} />
      </div>
    );
  }
}
Modal.propTypes = {
  show: PropTypes.bool,
  onClose: PropTypes.func,
  image: PropTypes.object,
  parentNode: PropTypes.object,
};

export default Modal;
