import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Modal from '../Modal';
import {
  loadImages,
  hideTopImages,
  showTopImages,
  resizeGrid,
} from '../../features/photos/actions';

const axios = require('axios').default;

class Grid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previousScrollY: 0,
      showImageFullScreen: false,
      image: null,
      fetching: false,
    };
    this.container = React.createRef();
  }

  componentDidMount() {
    if (this.container.current) {
      this.fetchTrendingGifs(0);
      window.addEventListener('scroll', this.onScroll, false);
      window.addEventListener('resize', this.updateSize);
    }
  }

  showModal = image => {
    this.setState(prevState => {
      return {
        image,
        showImageFullScreen: !prevState.showImageFullScreen,
      };
    });
  };

  updateSize = () => {
    if (this.state.timer) {
      clearTimeout(this.state.timer);
    }
    const timer = setTimeout(() => {
      this.props.resizeGrid(this.container.current.parentNode.offsetWidth);
    }, 50);
    this.setState({timer});
  };

  fetchTrendingGifs = async pageOffset => {
    if (this.props.offset != pageOffset) {
      try {
        const url = `${this.props.endpoint}&limit=50&offset=${pageOffset}`;
        const response = await axios.get(url);
        this.props.loadImages(
          response.data,
          4,
          this.container.current.parentNode.offsetWidth,
          200,
          pageOffset
        );
      } catch (error) {
        console.error(error);
      }
    }
  };

  onScroll = async () => {
    if (
      !this.state.fetching &&
      window.scrollY >= this.props.gridViewPortHeight * 0.5
    ) {
      this.setState({fetching: true});
      await this.fetchTrendingGifs(this.props.offset + 50);
      this.props.hideTopImages();
      this.setState({fetching: false});
    } else if (
      window.scrollY < this.state.previousScrollY &&
      window.scrollY <= this.props.gridViewPortHeight * 0.5
    ) {
      this.props.showTopImages();
    }

    if (this.state.previousScrollY !== window.scrollY) {
      this.setState({previousScrollY: window.scrollY});
    }
  };

  render() {
    const {grid} = this.props;

    return (
      <div ref={this.container}>
        <div style={{opacity: this.state.showImageFullScreen ? '0.2' : '1'}}>
          {grid.map(row =>
            row.images.map(image => (
              <img
                className="grid-image"
                onClick={() => {
                  this.showModal(image);
                }}
                alt={image.title}
                style={{
                  top: 0,
                  left: 0,
                  position: 'absolute',
                  transform: `translateX(${image.xStartPosition}px) translateY(${row.rowHeightStartPosition}px)`,
                }}
                key={image.url}
                src={image.url}
                height={`${image.height}px`}
                width={`${image.width}px`}
              />
            ))
          )}
        </div>
        {this.state.showImageFullScreen ? (
          <Modal
            parentNode={this.container.current.parentNode}
            image={this.state.image}
            onClose={this.showModal}
            show={this.state.showImageFullScreen}
          />
        ) : null}
      </div>
    );
  }
}

Grid.propTypes = {
  offset: PropTypes.number,
  grid: PropTypes.array,
  gridViewPortHeight: PropTypes.number,
  loadImages: PropTypes.func,
  hideTopImages: PropTypes.func,
  showTopImages: PropTypes.func,
  resizeGrid: PropTypes.func,
  endpoint: PropTypes.string,
};

const mapStateToProps = state => {
  return {
    offset: state.photo.offset,
    grid: state.photo.grid,
    gridViewPortHeight: state.photo.gridViewPortHeight,
    endpoint: state.photo.endpoint,
  };
};

const mapDispatchToProps = {
  loadImages,
  hideTopImages,
  showTopImages,
  resizeGrid,
};

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
